require 'rails_helper'

describe Comment, type: :model do
  subject { build(:comment) }

  describe 'factory' do
    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:post) }
  end

  describe 'validations' do
    it { should validate_presence_of(:content) }
    it { should validate_length_of(:content).is_at_least(15) }
  end
end
