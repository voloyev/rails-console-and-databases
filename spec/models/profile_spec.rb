require 'rails_helper'

describe Profile, type: :model do
  subject { build(:profile) }

  describe 'factory' do
    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_length_of(:first_name).is_at_least(2).is_at_most(20) }

    it { should validate_presence_of(:last_name) }
    it { should validate_length_of(:last_name).is_at_least(2).is_at_most(20) }

    it { should validate_presence_of(:age) }
    it { should validate_numericality_of(:age).only_integer }

    it { should validate_length_of(:bio).is_at_least(5).is_at_most(255).allow_nil }
  end
end
